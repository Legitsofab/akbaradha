$(document).ready(function() {
    $("#searchBar").keyup( function() {
      var isi = $("#searchBar").val();
      var urlp = '/story8/search/?q=' + isi;

      $.ajax({
        url: urlp,
        success: function(hasil) {
          var array_items = hasil.items;
          console.log(hasil);
          var obj_hasil = $("#daftar_isi");
          obj_hasil.empty();

          for (i = 0; i < array_items.length; i++) {
            var title = hasil.items[i].volumeInfo.title;
            var thumbnails = hasil.items[i].volumeInfo.imageLinks.smallThumbnail;
            var description = array_items[i].volumeInfo.description;
            var link = array_items[i].volumeInfo.previewLink;
            var image = "<img src=" + thumbnails + "class=" + "card-img" + ">";
            console.log(title);
            obj_hasil.append(
        "<br><div class=" + "card mb-3" + "style=" + "max-width:540px;border-radius:25px;" + ">" +
            "<div class=" + "row no-gutters" + ">" +
                "<div class=" + "col-md-4" + ">" +
                    image +
                "</div>" +
                "<div class=" + "col-md-8" + ">" +
                    "<div class=" + "card-body" + ">" +
                        "<h5 class=" + "card-title" + ">" + title + "</h5>" +
                        "<p class=" + "card-text" +">"+description + "</p>" +
                        "<a href=" + link + "class=" + "btn btn-primary" + ">" + "Link to source" + "</a>" +
                    "</div>" +
                "</div>" +
            "</div>" +
        "</div><br>");
          }
        }
      })
    })
});