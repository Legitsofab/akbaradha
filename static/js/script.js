$(document).ready(function() {
    var acc = document.getElementsByClassName("accordion");
    var i;
    
    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
    
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
          panel.style.display = "none";
        } else {
          panel.style.display = "block";
        }
      });
    }
    var upBut = document.getElementsByClassName("up");
    var downBut = document.getElementsByClassName("down");
    var j;
    var h;
    // UP BUTTON
    for (j = 0; j < upBut.length; j++) {
        upBut[j].addEventListener("click", function() {
            var self = $(this)
            var item = self.parents('div.container')
            var swapWith = item.prev();
          if (swapWith.hasClass("container")) {
            item.after(swapWith.detach())
          } else {
            
          }
        });
      }

    // DOWN BUTTON
    for (h = 0; h < downBut.length; h++) {
        downBut[h].addEventListener("click", function() {
          var self = $(this)
          var item = self.parents('div.container')
          var swapWith = item.next();
          if (swapWith.hasClass("container")) {
            item.before(swapWith.detach())
          } else {
            
          }
        });
      }
});