from django.urls import path
from . import views

app_name = 'story7'

urlpatterns = [
    path('',views.home7,name='home7'),
]