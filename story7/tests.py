from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import home7
from .apps import Story7Config

# Create your tests here.
class TestHome(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code,200)
    
    def test_event_index_func(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, home7)

    def test_template_used(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response,'Story7/AccordionPage.html')
        self.assertContains(response, 'Aktivitas saat ini')
        self.assertContains(response, 'Pengalaman organisasi/kepanitiaan')
        self.assertContains(response, 'Prestasi')

class TestApp(TestCase):
    def test_app_name(self):
        self.assertEqual(Story7Config.name,'story7')