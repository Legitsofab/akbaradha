from django.urls import path
from . import views

app_name = 'story5'

urlpatterns = [
    path('delete/<int:delete_id>',views.delete,name='delete'),
    path('update/<int:update_id>',views.update,name='update'),
    path('viewmatkul/<int:viewmatkul_id>',views.viewmatkul,name='viewmatkul'),
    path('tambah-matkul',views.form_matkul, name='form'),
    path('list-matkul',views.list_matkul, name='list_matkul'),
    path('story5',views.home_story5, name='story5'),
]