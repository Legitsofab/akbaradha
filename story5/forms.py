from django import forms
from .models import MatkulModel
from django.forms import TextInput, Select, Textarea

class MatkulForm(forms.ModelForm):
    class Meta:
        model = MatkulModel
        fields = [
            'nama_matkul',
            'dosen_pengajar',
            'ruang_matkul',
            'jumlah_sks',
            'deskripsi_matkul',
            'semester_tahun',
        ]

        widgets = {
                'nama_matkul'       : forms.TextInput(
                    attrs={
                        'class':'form-control',
                        'placeholder':'ex. Dasar-Dasar Pemrograman 2'
                        }
                    ),
                'dosen_pengajar'    : forms.TextInput(
                    attrs ={
                        'class':'form-control',
                        'placeholder':'Nama dosen pengajar'
                        }
                    ),
                'ruang_matkul'      : forms.TextInput(
                    attrs={
                        'class':'form-control',
                        'placeholder':'ex. 2.2502'
                        }
                    ),
                'jumlah_sks'        : forms.Select(
                    attrs ={
                        'class':'form-control col-lg-2',
                        }
                    ),
                'deskripsi_matkul'  : forms.Textarea(
                    attrs = {
                        'class':'form-control',
                        'placeholder':'Deskripsi mata kuliah'
                        }
                    ),
                'semester_tahun'    : forms.Select(
                    attrs = {
                        'class':'form-control col-lg-2',
                        }
                    )
            }
