from django.db import models
from django import forms

# Create your models here.
class MatkulModel(models.Model):
    nama_matkul         = models.CharField(max_length = 100)
    dosen_pengajar      = models.CharField(max_length = 100)
    ruang_matkul        = models.CharField(max_length = 100)
    jumlah_sks          = models.CharField(
        max_length = 2,
        choices = [
            ('1','1'),
            ('2','2'),
            ('3','3'),
            ('4','4'),
            ('5','5'),
            ('6','6'),
        ],
        default = 1,
    )
    deskripsi_matkul    = models.TextField()
    semester_tahun      = models.CharField(
        max_length = 20,
        choices = [
            ('Ganjil 2019/2020','Ganjil 2019/2020'),
            ('Gasal 2019/2020','Gasal 2019/2020'),
            ('Ganjil 2020/2021','Ganjil 2020/2021'),
            ('Gasal 2020/2021','Gasal 2020/2021'),
        ],
        default = 'Ganjil 2019/2020',
    )

    def __str__(self):
        return "{}.{}".format(self.id,self.nama_matkul)
    