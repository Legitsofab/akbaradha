from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect

from .forms import MatkulForm
from .models import MatkulModel

# Create your views here.
def viewmatkul(request,viewmatkul_id):
    matkul = MatkulModel.objects.get(id=viewmatkul_id)

    context = {
        'matkul':matkul,
    }

    return render(request,'viewmatkul.html', context)

def update(request,update_id):
    matkul = MatkulModel.objects.get(id=update_id)

    data = {
        'nama_matkul'       : matkul.nama_matkul,
        'dosen_pengajar'    : matkul.dosen_pengajar,
        'ruang_matkul'      : matkul.ruang_matkul,
        'jumlah_sks'        : matkul.jumlah_sks,
        'deskripsi_matkul'  : matkul.deskripsi_matkul,
        'semester_tahun'    : matkul.semester_tahun
    }
    matkul_form = MatkulForm(request.POST or None, initial=data, instance=matkul)

    context = {
        'matkul_form':matkul_form,
    }
    if request.method == 'POST':
        if matkul_form.is_valid():
            matkul_form.save()
            return HttpResponseRedirect("/list-matkul")

    return render(request, 'form_matkul.html', context)
    

def delete(request,delete_id):
    MatkulModel.objects.filter(id=delete_id).delete()
    

def home_story5(request):
    return render(request, 'story5_home.html')

def form_matkul(request):
    matkul_form = MatkulForm(request.POST)
    context = {
        'matkul_form':matkul_form,
    }

    if request.method == 'POST':
        if matkul_form.is_valid():
            matkul_form.save()
            return HttpResponseRedirect("list-matkul")

    return render(request, 'form_matkul.html', context)

def list_matkul(request):
    Matkul = MatkulModel.objects.all()
    context = {
        'Matkul':Matkul,
    }
    return render(request, 'list_matkul.html', context)