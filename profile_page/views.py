from django.shortcuts import render

# Create your views here.
def landing_page(request):
    return render(request, 'LandingPage.html')

def projects_page(request):
    return render(request, 'ProjectPage.html')

def resume_page(request):
    return render(request, 'ResumePage.html')

def hobbies_page(request):
    return render(request, 'HobbiesPage.html')

def comingsoon_page(request):
    return render(request, 'NewPage.html')