from django.urls import path
from . import views

app_name = 'profile_page'

urlpatterns = [
    path('',views.landing_page, name='home'),
    path('home',views.landing_page, name='home2'),
    path('projects',views.projects_page, name='projects'),
    path('resume',views.resume_page, name='resume'),
    path('hobbies',views.hobbies_page, name='hobbies'),
    path('coming-soon',views.comingsoon_page, name='comingsoon'),
]