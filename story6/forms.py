from django import forms
from .models import KegiatanModel, PesertaModel

class KegiatanForm(forms.ModelForm):
    class Meta:
        model = KegiatanModel
        fields = [
            'nama_kegiatan'
        ]

        widget = {
            'nama_kegiatan'     : forms.TextInput(
                attrs={
                    'class' : 'form-control',
                    'placeholder' : 'ex. Maen Futsal'
                }
            )
        }

class PesertaForm(forms.ModelForm):
    class Meta:
        model = PesertaModel
        fields = [
            'nama_peserta'
        ]

    widgets = {
        'nama_peserta'      : forms.TextInput(
            attrs={
                'class' : 'form-control',
                'placeholder' : 'ex. Raul Gonzales',
            }
        )
    }