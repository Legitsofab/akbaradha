from django.test import TestCase, Client
from django.urls import resolve
from .views import home_story6, form_kegiatan, form_peserta, list_kegiatan, delete_kegiatan, delete_peserta
from .models import KegiatanModel, PesertaModel
from .apps import Story6Config
# Create your tests here.
class TestKegiatanHome(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/story6')
        self.assertEqual(response.status_code,200)

    def test_event_index_func(self):
        found = resolve('/story6')
        self.assertEqual(found.func, home_story6)

class TestKegiatanTambah(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/tambah-kegiatan')
        self.assertEqual(response.status_code,200)
    
    def test_event_index_func(self):
        found = resolve('/tambah-kegiatan')
        self.assertEqual(found.func, form_kegiatan)

    def test_event_model_create_new_object(self):
        kegiatan = KegiatanModel(nama_kegiatan="PPW")
        kegiatan.save()
        self.assertEqual(KegiatanModel.objects.all().count(), 1)
    
    def test_event_url_post_is_exist(self):
        response = Client().post(
            '/tambah-kegiatan', data={'nama_kegiatan': 'Coding'})
        self.assertEqual(response.status_code, 302)

class TestHapusKegiatan(TestCase):
    def test_str_is_equal_to_title(self):
        kegiatanTest = KegiatanModel(nama_kegiatan="kondangan")
        self.assertEqual(str(kegiatanTest),kegiatanTest.nama_kegiatan)

    def test_hapus_url_post_is_exist(self):
        response = Client().post('/delete-kegiatan/1')
        self.assertEqual(response.status_code,302)

class TestPesertaTambah(TestCase):
    def test_regist_url_is_exist(self):
        response = Client().get('/tambah-peserta/1')
        self.assertEqual(response.status_code, 200)


class TestHapusNama(TestCase):
    def test_str_is_equal_to_title(self):
        pesertaTest = PesertaModel(nama_peserta="kondangan")
        self.assertEqual(str(pesertaTest),pesertaTest.nama_peserta)

    def test_hapus_url_post_is_exist(self):
        response = Client().post('/delete-peserta/1')
        self.assertEqual(response.status_code,302)

class TestApp(TestCase):
    def test_app_name(self):
        self.assertEqual(Story6Config.name,'story6')