from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('delete-kegiatan/<int:delkeg_id>',views.delete_kegiatan,name='delkeg'),
    path('delete-peserta/<int:delpes_id>',views.delete_peserta,name='delpes'),
    path('tambah-kegiatan',views.form_kegiatan, name='form_kegiatan'),
    path('tambah-peserta/<int:kegiatan_id>',views.form_peserta, name='form_peserta'),
    path('list-kegiatan',views.list_kegiatan, name='list_kegiatan'),
    path('story6',views.home_story6, name='story6'),
]