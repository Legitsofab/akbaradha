from django.db import models

# Create your models here.
class KegiatanModel(models.Model):
    nama_kegiatan       = models.CharField(max_length = 100)

    def __str__(self):
        return self.nama_kegiatan


class PesertaModel(models.Model):
    nama_peserta        = models.CharField(max_length = 100)
    kegiatan            = models.ForeignKey(
        KegiatanModel, on_delete=models.CASCADE, null=True, blank=True
    )

    def __str__(self):
        return self.nama_peserta