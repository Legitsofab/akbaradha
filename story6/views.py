from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect

from .forms import KegiatanForm, PesertaForm
from .models import KegiatanModel, PesertaModel

# from .forms import KegiatanForm, PesertaForm
# from .models import KegiatanModel, PesertaModel
# Create your views here.
def home_story6(request):
    return render(request, 'story6_home.html')

def form_kegiatan(request):
    kegiatanForm = KegiatanForm(request.POST)
    context = {
        'kegiatanForm':kegiatanForm,
    }

    if request.method == 'POST':
        if kegiatanForm.is_valid():
            kegiatanForm.save()
            return HttpResponseRedirect('/list-kegiatan')

    return render(request, 'form_kegiatan.html', context)

def form_peserta(request, kegiatan_id):
    pesertaForm = PesertaForm(request.POST)
    context = {
        'pesertaForm':pesertaForm,
    }

    if request.method == 'POST':
        if pesertaForm.is_valid():
            peserta = PesertaModel(kegiatan=KegiatanModel.objects.get(id=kegiatan_id),
                nama_peserta = pesertaForm.data['nama_peserta'])
            peserta.save()
            return HttpResponseRedirect('/list-kegiatan')

    return render(request, 'form_peserta.html', context)

def list_kegiatan(request):
    Kegiatan = KegiatanModel.objects.all()
    Peserta = PesertaModel.objects.all()

    context = {
        'Kegiatan':Kegiatan,
        'Peserta':Peserta,
    }
    return render(request, 'list_kegiatan.html', context)

def delete_kegiatan(request, delkeg_id):
    KegiatanModel.objects.filter(id=delkeg_id).delete()
    return HttpResponseRedirect('/list-kegiatan')

def delete_peserta(request, delpes_id):
    PesertaModel.objects.filter(id=delpes_id).delete()
    return HttpResponseRedirect('/list-kegiatan')