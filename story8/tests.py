from django.test import TestCase, Client
from django.urls import resolve
from .views import home8,home8b
from .apps import Story8Config

# Create your tests here.
class TestStory8Home(TestCase):
    def test_index_url_is_exist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code,200)
    
    def test_index_func(self):
        found = resolve('/story8/')
        self.assertEqual(found.func,home8b)

    def test_index_template(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'Story8/BookSearch.html')

class TestStory8Search(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/story8/search/?q=testbook')
        self.assertEqual(response.status_code, 200)

class TestApp(TestCase):
    def test_app_name(self):
        self.assertEqual(Story8Config.name,'story8')