from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.
def home8b(request):
    return render(request,'Story8/BookSearch.html')

def home8(request):
    word = request.GET['q']
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + word
    req = requests.get(url)
    data = json.loads(req.content)
    return JsonResponse(data,safe=False)