from django.urls import path
from . import views

app_name = 'story8'

urlpatterns = [
    path('',views.home8b,name='home8b'),
    path('search/',views.home8,name='home8'),
]